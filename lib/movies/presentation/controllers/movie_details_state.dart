import 'package:equatable/equatable.dart';
import 'package:watchit/core/utils/enum.dart';
import 'package:watchit/movies/domain/entities/movie_details.dart';
import 'package:watchit/movies/domain/entities/recommendation.dart';

class MovieDetailsState extends Equatable {
  const MovieDetailsState(
      {this.movieRecommendatios = const [],
      this.movieRecommendatiosState = RequestState.loading,
      this.movieRecommendatiosmassage = '',
      this.movieDetailsState = RequestState.loading,
      this.movieDetailsmassage = '',
      this.movieDetails});

  final MovieDetails? movieDetails;
  final RequestState movieDetailsState;
  final String movieDetailsmassage;
  final List<MovieRecommendtion> movieRecommendatios;
  final RequestState movieRecommendatiosState;
  final String movieRecommendatiosmassage;
  MovieDetailsState copyWith({
    MovieDetails? movieDetails,
    RequestState? movieDetailsState,
    String? movieDetailsmassage,
    List<MovieRecommendtion>? movieRecommendatios,
    RequestState? movieRecommendatiosState,
    String? movieRecommendatiosmassage,
  }) {
    return MovieDetailsState(
        movieDetails: movieDetails ?? this.movieDetails,
        movieDetailsState: movieDetailsState ?? this.movieDetailsState,
        movieDetailsmassage: movieDetailsmassage ?? this.movieDetailsmassage,
        movieRecommendatios: movieRecommendatios ?? this.movieRecommendatios,
        movieRecommendatiosState:
            movieRecommendatiosState ?? this.movieRecommendatiosState,
        movieRecommendatiosmassage:
            movieRecommendatiosmassage ?? this.movieRecommendatiosmassage);
  }

  @override
  List<Object?> get props => [
        movieDetails,
        movieDetailsState,
        movieDetailsmassage,
        movieRecommendatios,
        movieRecommendatiosState,
        movieRecommendatiosmassage
      ];
}
