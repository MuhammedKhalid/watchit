import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:watchit/core/utils/enum.dart';

import 'package:watchit/movies/domain/usecases/get_movie_details_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_recommendations_usecase.dart';
import 'package:watchit/movies/presentation/controllers/movie_details_state.dart';

part 'movie_details_event.dart';

class MovieDetailsBloc extends Bloc<MovieDetailsEvent, MovieDetailsState> {
  MovieDetailsBloc(
      this.getMovieDetailsUseCase, this.getMovieRecommendationsUseCase)
      : super(const MovieDetailsState()) {
    on<GetMovieDetailsEvent>(_getMovieDetails);
    on<GetMovieRecomendationEvent>(_getMovieRecommendations);
  }

  final GetMovieDetailsUseCase getMovieDetailsUseCase;
  final GetMovieRecommendationsUseCase getMovieRecommendationsUseCase;
  FutureOr<void> _getMovieDetails(
      GetMovieDetailsEvent event, Emitter<MovieDetailsState> emit) async {
    final result = await getMovieDetailsUseCase(MovieDetailsParameters(
        movieID: event.id)); // movie id  // i add it to event before

    result.fold(
        (l) => emit(state.copyWith(
            movieDetailsState: RequestState.error,
            movieDetailsmassage: l.massage)),
        (r) => emit(state.copyWith(
              movieDetails: r,
              movieDetailsState: RequestState.loaded,
            )));
  }

  FutureOr<void> _getMovieRecommendations(
      GetMovieRecomendationEvent event, Emitter<MovieDetailsState> emit) async {
    final result = await getMovieRecommendationsUseCase(
        MovieRecommendatiosParameters(
            event.id)); // movie id  // i add it to event before

    result.fold(
        (l) => emit(state.copyWith(
            movieRecommendatiosState: RequestState.error,
            movieDetailsmassage: l.massage)),
        (r) => emit(state.copyWith(
              movieRecommendatios: r,
              movieRecommendatiosState: RequestState.loaded,
            )));
  }
}
