import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:watchit/core/utils/enum.dart';
import 'package:watchit/movies/domain/usecases/get_now_playing_movies_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_popular_movies_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_top_rated_movies_usecase.dart';
import 'package:watchit/movies/presentation/controllers/movies_event.dart';
import 'package:watchit/movies/presentation/controllers/movies_state.dart';

import '../../../core/usecase/base_usecase.dart';

class MoviesBloc extends Bloc<MoviesEvent, MoviesState> {
  final GetNowPlayingMoviesUseCase getNowPlayingMoviesUseCase;
  final GetPopularMoveisUseCase getPopularMoveisUseCase;
  final GetTopRatedMoviesUseCase getTopRatedMoviesUseCase;
  MoviesBloc(this.getNowPlayingMoviesUseCase, this.getPopularMoveisUseCase,
      this.getTopRatedMoviesUseCase)
      : super(const MoviesState()) {
    on<GetNowPlayingMoviesEvent>(_getNowPlayingMovies);

    on<GetPopularMoviesEvent>(_getPopularMovies);

    on<GetTopRatedMoviesEvent>(_getTopRatedMovies);
  }

  Future<void> _getNowPlayingMovies(
      GetNowPlayingMoviesEvent event, Emitter<MoviesState> emit) async {
    final result = await getNowPlayingMoviesUseCase(const NoParameters());
    result.fold(
        (l) => emit(state.copywith(
            nowPlayingState: RequestState.error,
            nowPlayingMoviesmassage: l.massage)),
        (r) => emit(state.copywith(
            nowPlayingMovies: r, nowPlayingState: RequestState.loaded)));
  }

  Future<void> _getPopularMovies(
      GetPopularMoviesEvent event, Emitter<MoviesState> emit) async {
    final result = await getPopularMoveisUseCase(const NoParameters());
    result.fold(
        (l) => emit(state.copywith(
            popularState: RequestState.error, popularMoviesmassage: l.massage)),
        (r) => emit(state.copywith(
            popularMovies: r, popularState: RequestState.loaded)));
  }

  Future<void> _getTopRatedMovies(
      GetTopRatedMoviesEvent event, Emitter<MoviesState> emit) async {
    final result = await getTopRatedMoviesUseCase(const NoParameters());
    result.fold(
        (l) => emit(
              state.copywith(
                topRatedMoviesmassage: l.massage,
                topRatedState: RequestState.error,
              ),
            ),
        (r) => emit(
              state.copywith(
                  topRatedMovies: r, topRatedState: RequestState.loaded),
            ));
  }
}
