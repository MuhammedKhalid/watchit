import 'package:equatable/equatable.dart';
import 'package:watchit/core/utils/enum.dart';

import '../../domain/entities/movie.dart';

class MoviesState extends Equatable {
  final List<Movie> nowPlayingMovies;
  final RequestState nowPlayingState;
  final String nowPlayingMoviesmassage;

  final List<Movie> popularMovies;
  final RequestState popularState;
  final String popularMoviesmassage;

  final List<Movie> topRatedMovies;
  final RequestState topRatedState;
  final String topRatedMoviesmassage;

  const MoviesState({
    this.nowPlayingMovies = const [],
    this.nowPlayingState = RequestState.loading,
    this.nowPlayingMoviesmassage = '',
    this.popularMovies = const [],
    this.popularState = RequestState.loading,
    this.popularMoviesmassage = '',
    this.topRatedMovies = const [],
    this.topRatedState = RequestState.loading,
    this.topRatedMoviesmassage = '',
  });

  MoviesState copywith({
    List<Movie>? nowPlayingMovies,
    RequestState? nowPlayingState,
    String? nowPlayingMoviesmassage,
    List<Movie>? popularMovies,
    RequestState? popularState,
    String? popularMoviesmassage,
    List<Movie>? topRatedMovies,
    RequestState? topRatedState,
    String? topRatedMoviesmassage,
  }) {
    return MoviesState(
        nowPlayingMovies: nowPlayingMovies ?? this.nowPlayingMovies,
        nowPlayingState: nowPlayingState ?? this.nowPlayingState,
        nowPlayingMoviesmassage:
            nowPlayingMoviesmassage ?? this.nowPlayingMoviesmassage,
        popularMovies: popularMovies ?? this.popularMovies,
        popularState: popularState ?? this.popularState,
        popularMoviesmassage: popularMoviesmassage ?? this.popularMoviesmassage,
        topRatedMovies: topRatedMovies ?? this.topRatedMovies,
        topRatedState: topRatedState ?? this.topRatedState,
        topRatedMoviesmassage:
            topRatedMoviesmassage ?? this.topRatedMoviesmassage);
  }

  @override
  List<Object?> get props => [
        nowPlayingMovies,
        nowPlayingState,
        nowPlayingMoviesmassage,
        popularMovies,
        popularState,
        popularMoviesmassage,
        topRatedMovies,
        topRatedState,
        topRatedMoviesmassage
      ];
}
