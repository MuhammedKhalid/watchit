import 'package:watchit/movies/domain/entities/recommendation.dart';

class MovieRecommendationsModel extends MovieRecommendtion {
  const MovieRecommendationsModel({super.backdropPath, required super.id});

  factory MovieRecommendationsModel.fromJson(Map<String, dynamic> json) =>
      MovieRecommendationsModel(
          backdropPath:
              json['backdrop_path'] ?? '/a2tys4sD7xzVaogPntGsT1ypVoT.jpg',
          id: json['id']);
}
