import 'package:dartz/dartz.dart';
import 'package:watchit/error/exceptions.dart';
import 'package:watchit/error/failure.dart';
import 'package:watchit/movies/data/datasource/movie_remote_data_source.dart';
import 'package:watchit/movies/domain/entities/movie.dart';
import 'package:watchit/movies/domain/entities/movie_details.dart';
import 'package:watchit/movies/domain/entities/recommendation.dart';
import 'package:watchit/movies/domain/repository/base_movies_repository.dart';
import 'package:watchit/movies/domain/usecases/get_movie_details_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_recommendations_usecase.dart';

class MoviesRepository extends BaseMoveisRepository {
  final BaseMovieRemoteDataSource baseMovieRemoteDataSource;

  MoviesRepository(this.baseMovieRemoteDataSource);
  @override
  Future<Either<Failure, List<Movie>>> getNowPlaying() async {
    try {
      final result = await baseMovieRemoteDataSource.getNowPlayingMovies();
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorMassageModel.statusMassage));
    }
  }

  @override
  Future<Either<Failure, List<Movie>>> getPopularMovies() async {
    try {
      final result = await baseMovieRemoteDataSource.getPopularMovies();
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorMassageModel.statusMassage));
    }
  }

  @override
  Future<Either<Failure, List<Movie>>> getTopRatedMovies() async {
    try {
      final result = await baseMovieRemoteDataSource.getTopRatedMovies();
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorMassageModel.statusMassage));
    }
  }

  @override
  Future<Either<Failure, MovieDetails>> getMovieDetails(
      MovieDetailsParameters parameters) async {
    try {
      final result =
          await baseMovieRemoteDataSource.getMovieDetails(parameters);
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorMassageModel.statusMassage));
    }
  }

  @override
  Future<Either<Failure, List<MovieRecommendtion>>> getMovieRecommendations(
      MovieRecommendatiosParameters parameters) async {
    try {
      final result =
          await baseMovieRemoteDataSource.getMovieRecommendatios(parameters);
      return Right(result);
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorMassageModel.statusMassage));
    }
  }
}
