import 'package:dio/dio.dart';
import 'package:watchit/core/network/api_constance.dart';
import 'package:watchit/core/network/error_massage_model.dart';
import 'package:watchit/error/exceptions.dart';
import 'package:watchit/movies/data/models/movie_details_model.dart';
import 'package:watchit/movies/data/models/movie_model.dart';
import 'package:watchit/movies/data/models/movie_recommendatios_model.dart';
import 'package:watchit/movies/domain/usecases/get_movie_details_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_recommendations_usecase.dart';

abstract class BaseMovieRemoteDataSource {
  Future<List<MovieModel>> getNowPlayingMovies();
  Future<List<MovieModel>> getPopularMovies();
  Future<List<MovieModel>> getTopRatedMovies();
  Future<MovieDetailsModel> getMovieDetails(MovieDetailsParameters parameters);
  Future<List<MovieRecommendationsModel>> getMovieRecommendatios(
      MovieRecommendatiosParameters parameters);
}

class MovieRemoteDataSource extends BaseMovieRemoteDataSource {
  @override
  Future<List<MovieModel>> getNowPlayingMovies() async {
    final response = await Dio().get(ApiConstance.nowPlayingMoviesPath);
    if (response.statusCode == 200) {
      return List<MovieModel>.from((response.data['results'] as List)
          .map((e) => MovieModel.fromjson(e)));
    } else {
      throw ServerException(ErrorMassageModel.fromJson(response.data));
    }
  }

  @override
  Future<List<MovieModel>> getPopularMovies() async {
    final response = await Dio().get(ApiConstance.popularMoviesPath);

    if (response.statusCode == 200) {
      return List<MovieModel>.from((response.data['results'] as List)
          .map((e) => MovieModel.fromjson(e)));
    } else {
      throw ServerException(ErrorMassageModel.fromJson(response.data));
    }
  }

  @override
  Future<List<MovieModel>> getTopRatedMovies() async {
    final response = await Dio().get(ApiConstance.topRatedMoviesPath);

    if (response.statusCode == 200) {
      return List<MovieModel>.from((response.data['results'] as List)
          .map((e) => MovieModel.fromjson(e)));
    } else {
      throw ServerException(ErrorMassageModel.fromJson(response.data));
    }
  }

  @override
  Future<MovieDetailsModel> getMovieDetails(
      MovieDetailsParameters parameters) async {
    final response =
        await Dio().get(ApiConstance.movieDetailsPath(parameters.movieID));
    if (response.statusCode == 200) {
      return MovieDetailsModel.fromJson(response.data);
    } else {
      throw ServerException(ErrorMassageModel.fromJson(response.data));
    }
  }

  @override
  Future<List<MovieRecommendationsModel>> getMovieRecommendatios(
      MovieRecommendatiosParameters parameters) async {
    final response =
        await Dio().get(ApiConstance.movieRecomendationsPath(parameters.id));
    if (response.statusCode == 200) {
      return List<MovieRecommendationsModel>.from(
          (response.data['results'] as List)
              .map((e) => MovieRecommendationsModel.fromJson(e)));
    } else {
      throw ServerException(ErrorMassageModel.fromJson(response.data));
    }
  }
}
