import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:watchit/core/usecase/base_usecase.dart';
import 'package:watchit/error/failure.dart';
import 'package:watchit/movies/domain/entities/recommendation.dart';
import 'package:watchit/movies/domain/repository/base_movies_repository.dart';

class GetMovieRecommendationsUseCase extends BaseUseCase<
    List<MovieRecommendtion>, MovieRecommendatiosParameters> {
  GetMovieRecommendationsUseCase(this.baseMoveisRepository);
  final BaseMoveisRepository baseMoveisRepository;

  @override
  Future<Either<Failure, List<MovieRecommendtion>>> call(
      MovieRecommendatiosParameters parameters) async {
    return await baseMoveisRepository.getMovieRecommendations(parameters);
  }
}

class MovieRecommendatiosParameters extends Equatable {
  final int id;

  const MovieRecommendatiosParameters(this.id);

  @override
  List<Object?> get props => [id];
}
