import 'package:dartz/dartz.dart';
import 'package:watchit/core/usecase/base_usecase.dart';
import 'package:watchit/movies/domain/entities/movie.dart';
import 'package:watchit/movies/domain/repository/base_movies_repository.dart';

import '../../../error/failure.dart';

class GetPopularMoveisUseCase extends BaseUseCase<List<Movie>,NoParameters> {
  final BaseMoveisRepository baseMoveisRepository;

  GetPopularMoveisUseCase(this.baseMoveisRepository);

  @override
  Future<Either<Failure, List<Movie>>> call(NoParameters parameters) async {
    return await baseMoveisRepository.getPopularMovies();
  }
}
