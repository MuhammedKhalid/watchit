import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:watchit/core/usecase/base_usecase.dart';
import 'package:watchit/error/failure.dart';
import 'package:watchit/movies/domain/entities/movie_details.dart';
import 'package:watchit/movies/domain/repository/base_movies_repository.dart';

class GetMovieDetailsUseCase
    extends BaseUseCase<MovieDetails, MovieDetailsParameters> {
  final BaseMoveisRepository baseMoveisRepository;
  GetMovieDetailsUseCase(this.baseMoveisRepository);

  @override
  Future<Either<Failure, MovieDetails>> call(
      MovieDetailsParameters parameters) async {
    return await baseMoveisRepository.getMovieDetails(parameters);
  }
}

class MovieDetailsParameters extends Equatable {
  final int movieID;

  const MovieDetailsParameters({required this.movieID});

  @override
  List<Object?> get props => [movieID];
}
