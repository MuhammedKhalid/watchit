import 'package:equatable/equatable.dart';
import 'package:watchit/movies/domain/entities/genres.dart';

class MovieDetails extends Equatable {
  final int id;
  final List<Genres> genres;
  final int runtime;
  final String title;
  final String overview;
  final double voteAverage;
  final String releaseDate;
  final String backdropPath;

  const MovieDetails({required this.id,required this.runtime,required this.title,required this.overview,
     required this.voteAverage,required this.releaseDate,required this.backdropPath,required this.genres});

  @override
  List<Object?> get props =>
      [id, runtime, title, overview, voteAverage, releaseDate, backdropPath];
}
