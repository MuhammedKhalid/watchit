import 'package:equatable/equatable.dart';

class Movie extends Equatable {
  final int id;
  final String title;
  final String backdropPath;
  final List<int> genderIds;
  final String overView;
  final double voteAverage;
  final String releaseDate;

  const Movie(
      {required this.id,
      required this.title,
      required this.backdropPath,
      required this.genderIds,
      required this.overView,
      required this.voteAverage,
      required this.releaseDate});

  @override
  List<Object?> get props =>
      [id, title, backdropPath, genderIds, overView, voteAverage, releaseDate];
}
