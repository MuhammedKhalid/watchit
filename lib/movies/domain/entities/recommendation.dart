import 'package:equatable/equatable.dart';

class MovieRecommendtion extends Equatable {
  final String? backdropPath;
  final int id;

  const MovieRecommendtion({this.backdropPath, required this.id});

  @override
  List<Object?> get props => [backdropPath, id];
}
