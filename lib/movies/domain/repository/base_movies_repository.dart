import 'package:dartz/dartz.dart';
import 'package:watchit/movies/domain/entities/movie.dart';
import 'package:watchit/movies/domain/entities/movie_details.dart';
import 'package:watchit/movies/domain/entities/recommendation.dart';
import 'package:watchit/movies/domain/usecases/get_movie_details_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_recommendations_usecase.dart';

import '../../../error/failure.dart';

abstract class BaseMoveisRepository {
  Future<Either<Failure, List<Movie>>> getNowPlaying();
  Future<Either<Failure, List<Movie>>> getPopularMovies();
  Future<Either<Failure, List<Movie>>> getTopRatedMovies();

  Future<Either<Failure, MovieDetails>> getMovieDetails(
      MovieDetailsParameters parameters);

  Future<Either<Failure, List<MovieRecommendtion>>> getMovieRecommendations(
      MovieRecommendatiosParameters parameters);
}
