import 'package:flutter/material.dart';
import 'package:watchit/core/services/services_locator.dart';
import 'package:watchit/core/utils/app_string.dart';
import 'package:watchit/movies/presentation/screens/movies_screen.dart';

void main() {
  ServicesLocator().init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppString.appName,
      theme: ThemeData.dark().copyWith(
        backgroundColor: Colors.grey.shade900,
      ),
      home: const MoviesScreen(),
    );
  }
}
