import 'package:get_it/get_it.dart';
import 'package:watchit/movies/data/datasource/movie_remote_data_source.dart';
import 'package:watchit/movies/data/repository/movies_repository.dart';
import 'package:watchit/movies/domain/repository/base_movies_repository.dart';
import 'package:watchit/movies/domain/usecases/get_movie_details_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_now_playing_movies_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_popular_movies_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_recommendations_usecase.dart';
import 'package:watchit/movies/domain/usecases/get_top_rated_movies_usecase.dart';
import 'package:watchit/movies/presentation/controllers/movie_details_bloc.dart';
import 'package:watchit/movies/presentation/controllers/movies_bloc.dart';

final sl = GetIt.instance;

class ServicesLocator {
  void init() {
    // BLOC
    sl.registerFactory(() => MoviesBloc(sl(), sl(), sl()));
    sl.registerFactory(() => MovieDetailsBloc(sl(), sl()));

    /// USECASE
    sl.registerLazySingleton(() => GetNowPlayingMoviesUseCase(sl()));
    sl.registerLazySingleton(() => GetPopularMoveisUseCase(sl()));
    sl.registerLazySingleton(() => GetTopRatedMoviesUseCase(sl()));
    sl.registerLazySingleton(() => GetMovieDetailsUseCase(sl()));
    sl.registerLazySingleton(() => GetMovieRecommendationsUseCase(sl()));

    ///REPOSITORY
    sl.registerLazySingleton<BaseMoveisRepository>(
        () => MoviesRepository(sl()));

    /// DATA SOURCE
    sl.registerLazySingleton<BaseMovieRemoteDataSource>(
        () => MovieRemoteDataSource());
  }
}
