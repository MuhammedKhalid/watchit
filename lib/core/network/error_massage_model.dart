import 'package:equatable/equatable.dart';

class ErrorMassageModel extends Equatable {
  @override
  List<Object?> get props => [statusCode, statusMassage, success];
  final int statusCode;
  final String statusMassage;
  final bool success;

  const ErrorMassageModel(this.statusCode, this.statusMassage, this.success);

  factory ErrorMassageModel.fromJson(Map<String, dynamic> json) {
    return ErrorMassageModel(
        json['status_code'], json[';status_message'], json['success']);
  }
}
