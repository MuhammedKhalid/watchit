import 'package:watchit/movies/domain/entities/movie.dart';

List<Movie> moviesList = [
  const Movie(
      id: 453395,
      title: "Movie Title",
      backdropPath: "/nmGWzTLMXy9x7mKd8NKPLmHtWGa.jpg",
      genderIds: [
        14,
        28,
        12,
      ],
      overView:
          "Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.",
      voteAverage: 5.0,
      releaseDate: "2022-05-04"),
  const Movie(
      id: 453395,
      title: "Movie Title",
      backdropPath: "/nmGWzTLMXy9x7mKd8NKPLmHtWGa.jpg",
      genderIds: [
        14,
        28,
        12,
      ],
      overView:
          "Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.",
      voteAverage: 5.0,
      releaseDate: "2022-05-04"),
  const Movie(
      id: 453395,
      title: "Movie Title",
      backdropPath: "/nmGWzTLMXy9x7mKd8NKPLmHtWGa.jpg",
      genderIds: [
        14,
        28,
        12,
      ],
      overView:
          "Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.",
      voteAverage: 5.0,
      releaseDate: "2022-05-04"),
  const Movie(
      id: 453395,
      title: "Movie Title",
      backdropPath: "/nmGWzTLMXy9x7mKd8NKPLmHtWGa.jpg",
      genderIds: [
        14,
        28,
        12,
      ],
      overView:
          "Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.",
      voteAverage: 5.0,
      releaseDate: "2022-05-04"),
  const Movie(
      id: 453395,
      title: "Movie Title",
      backdropPath: "/nmGWzTLMXy9x7mKd8NKPLmHtWGa.jpg",
      genderIds: [
        14,
        28,
        12,
      ],
      overView:
          "Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.",
      voteAverage: 5.0,
      releaseDate: "2022-05-04"),
  const Movie(
      id: 453395,
      title: "Movie Title",
      backdropPath: "/nmGWzTLMXy9x7mKd8NKPLmHtWGa.jpg",
      genderIds: [
        14,
        28,
        12,
      ],
      overView:
          "Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.",
      voteAverage: 5.0,
      releaseDate: "2022-05-04"),
];
