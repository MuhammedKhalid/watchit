class AppString {
  static const appName = ' Movies App';
  static const popular = 'Popular';
  static const seeMore = 'See More';
  static const topRated = 'Top Rated';
  static const nowPlaying = 'Now Playing';
  static const moreLikeThis = 'More like this';
  static const genres = 'Genres';
}
